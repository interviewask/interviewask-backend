# InterviewAsk - Backend (API)

API Backend server for InterviewAsk Application


### Start the web server:

   revel run InterviewAskApi

### Go to http://localhost:9000/ and you'll see:

    "It works"

## Some useful command used in this project:

### * Setting GOPATH

    export PATH=$PATH:/usr/local/go/bin

### * Make a directory for your go applications

    cd /home/apps/go/BscProject
    export GOPATH=/home/apps/go/BscProject

### * Install Revel.go using the following command

    go install github.com/revel/cmd/revel@latest
    go install github.com/revel/cmd/revel@latest
    export PATH="$PATH:$GOPATH/bin"

### * Adding revel dependency in project

    go get github.com/revel/revel
    go get github.com/revel/modules/static

### * Adding MongoDB driver

    go get go.mongodb.org/mongo-driver

### * Adding JSON Web Token package

    go get package github.com/golang-jwt/jwt

### * launch.json (For debug source code)

    {
        // Use IntelliSense to learn about possible attributes.
        // Hover to view descriptions of existing attributes.
        // For more information, visit: https://go.microsoft.com/fwlink/?linkid=830387
        "version": "0.2.0",
        "configurations": [
            {
                "name": "Connect to server",
                "type": "go",
                "request": "launch",
                "mode": "debug",
                //"remotePath": "",
                //"port": 2345,
                //"host": "127.0.0.1",
                "env": {},
                "showLog": true,
                "program": "${workspaceRoot}/app/tmp/",
                "args": ["-importPath", "InterviewAskApi", "-srcPath", "${workspaceRoot}/..",  "-runMode", "dev"],
            },
        ],
        "go.delveConfig": {
            "debugAdapter": "legacy",
        }
    }

### * Config Nginx server 
sudo vi /etc/nginx/sites-available/default

sudo vi /etc/nginx/sites-enabled/default

    location /api/v1/ {
        # First attempt to serve request as file, then
        # as directory, then fall back to displaying a 404.
        #try_files $uri $uri/ =404;
        add_header 'Access-Control-Allow-Origin' '*' always;
        add_header 'Access-Control-Allow-Methods' 'DELETE, POST, GET, OPTIONS, PUT';
        add_header 'Access-Control-Allow-Headers' 'Origin, X-Requested-With, Content-Type, Accept';
        if ($request_method = OPTIONS) {
                return 200;
        }

        resolver 8.8.8.8;
        proxy_pass http://127.0.0.1:9000/;
    }

## Code Layout

The directory structure of a generated Revel application:

    conf/             Configuration directory
        app.conf      Main app configuration file
        routes        Routes definition file

    app/              App sources
        init.go       Interceptor registration
        controllers/  App controllers go here
        views/        Templates directory

    messages/         Message files

    public/           Public static assets
        css/          CSS files
        js/           Javascript files
        images/       Image files

    tests/            Test suites

## Jira intergration

Gitlab and Jira intergration sucessfully!!
