package model

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type UserModel struct {
	Id          primitive.ObjectID `bson:"_id" json:"user_id"`
	UserName    string             `bson:"user_name" json:"user_name"`
	Password    string             `bson:"password" json:"password"`
	Email       string             `bson:"email" json:"email"`
	Role        string             `bson:"role" json:"role"`
	CreatedDate string             `bson:"created_date" json:"created_date"`
	UpdatedDate string             `bson:"updated_date" json:"updated_date"`
}

type UserData struct {
	User []UserModel `json:"user"`
}

type CheckUserResponseResult struct {
	Status  int    `json:"status"`
	Message string `json:"message"`
}

type UserResponseResult struct {
	Status  int      `json:"status"`
	Message string   `json:"message"`
	Data    UserData `json:"data"`
}
