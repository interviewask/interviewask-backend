package model

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type QuestionModel struct {
	Id          primitive.ObjectID `bson:"_id" json:"question_id"`
	Question    string             `bson:"question" json:"question"`
	Answer      string             `bson:"answer" json:"answer"`
	CategoryId  string             `bson:"category_id" json:"category_id" `
	CreatedDate string             `bson:"created_date" json:"created_date"`
	UpdatedDate string             `bson:"updated_date" json:"updated_date"`
}

type QuestionData struct {
	Questions []QuestionModel `json:"questions"`
}

type QuestionResponseResult struct {
	Status  int          `json:"status"`
	Message string       `json:"message"`
	Data    QuestionData `json:"data"`
}
