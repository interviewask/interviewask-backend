package model

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type CategoryModel struct {
	Id           primitive.ObjectID `bson:"_id" json:"_id"`
	CategoryName string             `bson:"category_name" json:"category_name"`
	CreatedDate  string             `bson:"created_date" json:"created_date"`
	UpdatedDate  string             `bson:"updated_date" json:"updated_date"`
}

type CategoryData struct {
	Categories []CategoryModel `json:"categories"`
}

type CheckCategoryResponseResult struct {
	Status  int    `json:"status"`
	Message string `json:"message"`
}

type CategoryResponseResult struct {
	Status  int          `json:"status"`
	Message string       `json:"message"`
	Data    CategoryData `json:"data"`
}
