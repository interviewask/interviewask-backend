package model

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type CommentModel struct {
	Id             primitive.ObjectID `bson:"_id" json:"comment_id"`
	CommentContent string             `bson:"comment_content" json:"comment_content"`
	UserId         string             `bson:"user_id" json:"user_id"`
	QuestionId     string             `bson:"question_id" json:"question_id"`
	CreatedDate    string             `bson:"created_date" json:"created_date"`
	UpdatedDate    string             `bson:"updated_date" json:"updated_date"`
}

type CommentData struct {
	Comments []CommentModel `json:"comments"`
}

type CommentResponseResult struct {
	Status  int         `json:"status"`
	Message string      `json:"message"`
	Data    CommentData `json:"data"`
}
