package service

import (
	"InterviewAskApi/app/common"
	"InterviewAskApi/app/model"
	"context"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

func GetCategories(categoryId string) (categoriesLst []model.CategoryModel, err error) {
	var filter interface{}
	if categoryId != "" {
		id, _ := primitive.ObjectIDFromHex(categoryId)
		filter = bson.M{"_id": id}
	} else {
		filter = bson.D{}
	}

	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	var categories []model.CategoryModel
	cur := common.QueryData(ctx, common.CATEGORIES_COLLECTION, filter)
	er := cur.All(ctx, &categories)

	if er != nil {
		return nil, er
	} else {
		return categories, nil
	}
}

func FindByCategoryName(categoryName string) ([]model.CategoryModel, error) {
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	var category []model.CategoryModel
	cur := common.QueryData(ctx, common.CATEGORIES_COLLECTION, bson.M{"category_name": categoryName})
	err := cur.All(ctx, &category)
	if err != nil {
		return nil, err
	} else {
		return category, nil
	}
}

func InsertCategory(category model.CategoryModel) (insertedId interface{}, err error) {
	time_now := time.Now().UTC()

	data := bson.D{{Key: "category_name", Value: category.CategoryName},
		{Key: "created_date", Value: time_now.Format("2006/01/02")},
		{Key: "updated_date", Value: time_now.Format("2006/01/02")}}
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	id, er := common.InsertData(ctx, common.CATEGORIES_COLLECTION, data)

	if er != nil {
		return nil, er
	} else {
		return id, nil
	}
}

func UpdateCategory(categoryId string, category model.CategoryModel) (updatedCount interface{}, err error) {
	time_now := time.Now().UTC()

	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)

	id, _ := primitive.ObjectIDFromHex(categoryId)
	filter := bson.M{"_id": id}
	update := bson.D{{Key: "$set",
		Value: bson.D{{Key: "category_name", Value: category.CategoryName},
			{Key: "updated_date", Value: time_now.Format("2006/01/02")}}}}
	result, er := common.UpdateData(ctx, common.CATEGORIES_COLLECTION, filter, update)

	if er != nil {
		return nil, er
	} else {
		return result, nil
	}
}

func DeleteCategory(categoryId string) (deletedCount interface{}, err error) {
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	id, _ := primitive.ObjectIDFromHex(categoryId)
	filter := bson.M{"_id": id}

	result, er := common.DeleteData(ctx, common.CATEGORIES_COLLECTION, filter)

	if er != nil {
		return nil, er
	} else {
		return result, nil
	}
}
