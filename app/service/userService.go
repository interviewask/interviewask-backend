package service

import (
	"InterviewAskApi/app/common"
	"InterviewAskApi/app/model"
	"context"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

func FindByUserName(userName string) ([]model.UserModel, error) {
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	var user []model.UserModel
	cur := common.QueryData(ctx, common.USER_COLLECTION, bson.M{"user_name": userName})
	err := cur.All(ctx, &user)
	if err != nil {
		return nil, err
	} else {
		return user, nil
	}
}

func CheckUserExists(userName string) ([]model.UserModel, bool) {
	user, err := FindByUserName(userName)
	if err != nil {
		return user, false
	}
	if len(user) > 0 {
		return user, true
	}
	return user, false
}

func GetUsers() (userLst []model.UserModel, err error) {

	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)

	var user []model.UserModel
	cur := common.QueryData(ctx, common.USER_COLLECTION, bson.D{})
	er := cur.All(ctx, &user)

	if er != nil {
		return nil, er
	} else {
		return user, nil
	}
}

func GetUserById(userId string) (userLst []model.UserModel, err error) {

	id, _ := primitive.ObjectIDFromHex(userId)

	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)

	var user []model.UserModel
	cur := common.QueryData(ctx, common.USER_COLLECTION, bson.M{"_id": id})
	er := cur.All(ctx, &user)

	if er != nil {
		return nil, er
	} else {
		return user, nil
	}
}

func InsertUser(user model.UserModel) (insertedId interface{}, err error) {
	time_now := time.Now().UTC()
	password, _ := common.HashPassword(user.Password)

	var user_check, err_check = FindByUserName(user.UserName)

	if err_check != nil {
		return nil, err_check
	} else {
		if len(user_check) > 0 {
			return "-9", nil
		} else {
			data := bson.D{{Key: "user_name", Value: user.UserName},
				{Key: "password", Value: password},
				{Key: "role", Value: "user"},
				{Key: "email", Value: user.Email},
				{Key: "created_date", Value: time_now.Format("2006/01/02")},
				{Key: "updated_date", Value: time_now.Format("2006/01/02")}}
			ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
			id, er := common.InsertData(ctx, common.USER_COLLECTION, data)
			if er != nil {
				return nil, er
			} else {
				return id, nil
			}
		}
	}
}

func UpdateUser(userId string, user model.UserModel) (updatedCount interface{}, err error) {
	time_now := time.Now().UTC()
	password, _ := common.HashPassword(user.Password)

	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)

	id, _ := primitive.ObjectIDFromHex(userId)
	filter := bson.M{"_id": id}
	update := bson.D{{Key: "$set",
		Value: bson.D{{Key: "user_name", Value: user.UserName},
			{Key: "password", Value: password},
			{Key: "role", Value: user.Role},
			{Key: "email", Value: user.Email},
			{Key: "updated_date", Value: time_now.Format("2006/01/02")}}}}
	result, er := common.UpdateData(ctx, common.USER_COLLECTION, filter, update)

	if er != nil {
		return nil, er
	} else {
		return result, nil
	}
}

func DeleteUser(userId string) (deletedCount interface{}, err error) {
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	id, _ := primitive.ObjectIDFromHex(userId)
	filter := bson.M{"_id": id}

	result, er := common.DeleteData(ctx, common.USER_COLLECTION, filter)

	if er != nil {
		return nil, er
	} else {
		return result, nil
	}
}
