package service

import (
	"InterviewAskApi/app/common"
	"InterviewAskApi/app/model"
	"context"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

func GetComments(commentId string) (commentsLst []model.CommentModel, err error) {
	var filter interface{}
	if commentId != "" {
		id, _ := primitive.ObjectIDFromHex(commentId)
		filter = bson.M{"_id": id}
	} else {
		filter = bson.D{}
	}

	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)

	var comments []model.CommentModel
	cur := common.QueryData(ctx, common.COMMENTS_COLLECTION, filter)
	er := cur.All(ctx, &comments)

	if er != nil {
		return nil, er
	} else {
		return comments, nil
	}
}

func GetCommentsByQuestionId(questionId string) (commentsLst []model.CommentModel, err error) {
	var filter interface{}

	question_id, _ := primitive.ObjectIDFromHex(questionId)
	filter = bson.M{"question_id": question_id}

	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)

	var comments []model.CommentModel
	cur := common.QueryData(ctx, common.COMMENTS_COLLECTION, filter)
	er := cur.All(ctx, &comments)

	if er != nil {
		return nil, er
	} else {
		return comments, nil
	}
}

func InsertComment(comment model.CommentModel) (insertedId interface{}, err error) {
	time_now := time.Now().UTC()

	data := bson.D{{Key: "comment_content", Value: comment.CommentContent},
		{Key: "user_id", Value: comment.UserId},
		{Key: "question_id", Value: comment.QuestionId},
		{Key: "created_date", Value: time_now.Format("2006/01/02")},
		{Key: "updated_date", Value: time_now.Format("2006/01/02")}}
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	id, er := common.InsertData(ctx, common.COMMENTS_COLLECTION, data)

	if er != nil {
		return nil, er
	} else {
		return id, nil
	}

}

func UpdateComment(commentId string, comment model.CommentModel) (updatedCount interface{}, err error) {
	time_now := time.Now().UTC()

	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)

	id, _ := primitive.ObjectIDFromHex(commentId)
	filter := bson.M{"_id": id}
	update := bson.D{{Key: "$set",
		Value: bson.D{{Key: "comment_content", Value: comment.CommentContent},
			{Key: "user_id", Value: comment.UserId},
			{Key: "question_id", Value: comment.QuestionId},
			{Key: "updated_date", Value: time_now.Format("2006/01/02")}}}}
	result, er := common.UpdateData(ctx, common.COMMENTS_COLLECTION, filter, update)

	if er != nil {
		return nil, er
	} else {
		return result, nil
	}
}

func DeleteComment(commentId string) (updatedCount interface{}, err error) {
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	id, _ := primitive.ObjectIDFromHex(commentId)
	filter := bson.M{"_id": id}

	result, er := common.DeleteData(ctx, common.COMMENTS_COLLECTION, filter)

	if er != nil {
		return nil, er
	} else {
		return result, nil
	}
}
