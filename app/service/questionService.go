package service

import (
	"InterviewAskApi/app/common"
	"InterviewAskApi/app/model"
	"context"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

func GetQuestions(questionId string) (questionLst []model.QuestionModel, err error) {
	var filter interface{}
	if questionId != "" {
		id, _ := primitive.ObjectIDFromHex(questionId)
		filter = bson.M{"_id": id}
	} else {
		filter = bson.D{}
	}

	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)

	var questions []model.QuestionModel
	cur := common.QueryData(ctx, common.QUESTIONS_COLLECTION, filter)
	er := cur.All(ctx, &questions)

	if er != nil {
		return nil, er
	} else {
		return questions, nil
	}
}

func GetQuestionsByCategoryId(categoryId string) (questionLst []model.QuestionModel, err error) {

	var filter interface{}
	category_id, _ := primitive.ObjectIDFromHex(categoryId)
	filter = bson.M{"category_id": category_id}

	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)

	var questions []model.QuestionModel
	cur := common.QueryData(ctx, common.QUESTIONS_COLLECTION, filter)
	er := cur.All(ctx, &questions)

	if er != nil {
		return nil, er
	} else {
		return questions, nil
	}
}

func InsertNewQuestion(question model.QuestionModel) (insertedId interface{}, err error) {
	time_now := time.Now().UTC()

	data := bson.D{{Key: "question", Value: question.Question},
		{Key: "answer", Value: question.Answer},
		{Key: "category_id", Value: question.CategoryId},
		{Key: "created_date", Value: time_now.Format("2006/01/02")},
		{Key: "updated_date", Value: time_now.Format("2006/01/02")}}
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	id, er := common.InsertData(ctx, common.QUESTIONS_COLLECTION, data)

	if er != nil {
		return nil, er
	} else {
		return id, nil
	}
}

func UpdateQuestion(questionId string, question model.QuestionModel) (updatedCount interface{}, err error) {
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	time_now := time.Now().UTC()

	id, _ := primitive.ObjectIDFromHex(questionId)
	filter := bson.M{"_id": id}
	update := bson.D{{Key: "$set",
		Value: bson.D{{Key: "question", Value: question.Question}, {Key: "answer", Value: question.Answer},
			{Key: "category_id", Value: question.CategoryId},
			{Key: "updated_date", Value: time_now.Format("2006/01/02")}}}}
	result, er := common.UpdateData(ctx, common.QUESTIONS_COLLECTION, filter, update)

	if er != nil {
		return nil, er
	} else {
		return result, nil
	}
}

func UpdateCategoryQuestion(questionId string, question model.QuestionModel) (updatedCount interface{}, err error) {
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	time_now := time.Now().UTC()

	id, _ := primitive.ObjectIDFromHex(questionId)
	filter := bson.D{{Key: "_id", Value: id}}
	update := bson.D{{Key: "$set",
		Value: bson.D{{Key: "category_id", Value: question.CategoryId},
			{Key: "updated_date", Value: time_now.Format("2006/01/02")}}}}
	result, er := common.UpdateData(ctx, common.QUESTIONS_COLLECTION, filter, update)

	if er != nil {
		return nil, er
	} else {
		return result, nil
	}
}

func DeleteQuestion(questionId string) (deletedCount interface{}, err error) {
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	id, _ := primitive.ObjectIDFromHex(questionId)
	filter := bson.M{"_id": id}

	result, er := common.DeleteData(ctx, common.QUESTIONS_COLLECTION, filter)

	if err != nil {
		return nil, er
	} else {
		return result, nil
	}
}
