package common

import (
	"context"
	"log"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func QueryData(ctx context.Context, tableName string, filter interface{}, opts ...*options.FindOptions) *mongo.Cursor {
	client, err := mongo.NewClient(options.Client().ApplyURI(DB_CONNECTION))

	if err != nil {
		log.Fatal(err)
	}

	err = client.Connect(ctx)
	if err != nil {
		panic(err)
	}
	defer client.Disconnect(ctx)

	collection := client.Database(DB_NAME).Collection(tableName)
	var cur *mongo.Cursor
	var queryErr error
	if opts != nil {
		cur, queryErr = collection.Find(ctx, filter, opts[0])
	} else {
		cur, queryErr = collection.Find(ctx, filter)
	}

	if queryErr != nil {
		panic(queryErr)
	}
	return cur
}

func InsertData(ctx context.Context, tableName string, data interface{}, opts ...*options.InsertOneOptions) (id interface{}, err error) {
	client, err := mongo.NewClient(options.Client().ApplyURI(DB_CONNECTION))

	if err != nil {
		log.Fatal(err)
	}

	err = client.Connect(ctx)
	if err != nil {
		client.Disconnect(ctx)
		return nil, err
	}

	collection := client.Database(DB_NAME).Collection(tableName)
	if opts != nil {
		result, queryErr := collection.InsertOne(ctx, data, opts[0])
		if queryErr != nil {
			client.Disconnect(ctx)
			return nil, queryErr
		}
		return result.InsertedID, nil
	} else {
		result, queryErr := collection.InsertOne(ctx, data)
		if queryErr != nil {
			client.Disconnect(ctx)
			return nil, queryErr
		}
		return result.InsertedID, nil
	}
}

func UpdateData(ctx context.Context, tableName string, filter interface{}, updateData interface{}, opts ...*options.UpdateOptions) (ModifiedCount interface{}, err error) {
	client, err := mongo.NewClient(options.Client().ApplyURI(DB_CONNECTION))

	if err != nil {
		log.Fatal(err)
	}

	err = client.Connect(ctx)
	if err != nil {
		client.Disconnect(ctx)
		return nil, err
	}

	collection := client.Database(DB_NAME).Collection(tableName)
	if opts != nil {
		result, queryErr := collection.UpdateOne(ctx, filter, updateData, opts[0])
		if queryErr != nil {
			client.Disconnect(ctx)
			return nil, queryErr
		}

		return result.ModifiedCount, nil
	} else {
		result, queryErr := collection.UpdateOne(ctx, filter, updateData)
		if queryErr != nil {
			client.Disconnect(ctx)
			return nil, queryErr
		}

		return result.ModifiedCount, nil
	}

}

func DeleteData(ctx context.Context, tableName string, filter interface{}, opts ...*options.DeleteOptions) (DeletedCount interface{}, err error) {
	client, err := mongo.NewClient(options.Client().ApplyURI(DB_CONNECTION))

	if err != nil {
		log.Fatal(err)
	}

	err = client.Connect(ctx)
	if err != nil {
		client.Disconnect(ctx)
		return nil, err
	}

	collection := client.Database(DB_NAME).Collection(tableName)
	if opts != nil {
		result, err := collection.DeleteOne(ctx, filter, opts[0])
		if err != nil {
			client.Disconnect(ctx)
			return nil, err
		}

		return result.DeletedCount, nil
	} else {
		result, err := collection.DeleteOne(ctx, filter)
		if err != nil {
			client.Disconnect(ctx)
			return nil, err
		}

		return result.DeletedCount, nil
	}
}
