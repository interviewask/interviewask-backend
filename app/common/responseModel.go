package common

type InsertModel struct {
	InsertedId interface{} `json:"inserted_id"`
}

type UpdateModel struct {
	UpdatedCount interface{} `json:"updated_count"`
}

type DeleteModel struct {
	DeletedCount interface{} `json:"deleted_count"`
}

type InsertResponseResult struct {
	Status  int         `json:"status"`
	Message string      `json:"message"`
	Data    InsertModel `json:"data"`
}

type UpdateResponseResult struct {
	Status  int         `json:"status"`
	Message string      `json:"message"`
	Data    UpdateModel `json:"data"`
}

type DeleteResponseResult struct {
	Status  int         `json:"status"`
	Message string      `json:"message"`
	Data    DeleteModel `json:"data"`
}
