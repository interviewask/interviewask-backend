package common

const DB_CONNECTION string = "mongodb+srv://duy1st1:duy1994789@cluster0.otuytoe.mongodb.net/?retryWrites=true&w=majority&appName=Cluster0"
const DB_NAME string = "MainDB1"
const QUESTIONS_COLLECTION string = "questions"
const CATEGORIES_COLLECTION string = "categories"
const COMMENTS_COLLECTION string = "comments"
const USER_COLLECTION string = "user"

const ADMIN_USERNAME string = "admin"
const ADMIN_PASSWORD string = "admin"
const ADMIN_ROLE string = "admin"
const SECRET_KEY string = "InterviewAskAdmin"

var UNAUTHORIZED_METHODS = []string{"GetQuestions", "GetCategories", "GetComments", "InsertComment", "UpdateComment", "DeleteComment", "CheckUserName", "GetUserById"}
var ADMIN_METHODS = []string{"InsertQuestion", "UpdateQuestion", "DeleteQuestion", "InsertCategory", "UpdateCategory", "DeleteCategory", "GetUsers"}
