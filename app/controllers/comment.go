package controllers

import (
	"InterviewAskApi/app/common"
	"InterviewAskApi/app/model"
	"InterviewAskApi/app/service"
	"net/http"

	"github.com/revel/revel"
)

type Comment struct {
	*revel.Controller
}

func (c Comment) GetComments() revel.Result {

	c.Params.Query = c.Request.URL.Query()
	var commentId string
	c.Params.Bind(&commentId, "comment_id")

	comments, err := service.GetComments(commentId)
	var responseResult model.CommentResponseResult

	if err != nil {
		c.Response.Status = http.StatusInternalServerError
		responseResult.Status = http.StatusInternalServerError
		responseResult.Message = err.Error()
		return c.RenderJSON(responseResult)
	} else {
		c.Response.Status = http.StatusOK
		c.Request.Header.Add("Content-Type", "application/json")
		responseResult.Status = http.StatusOK
		responseResult.Data.Comments = comments
		return c.RenderJSON(responseResult)
	}

}

func (c Comment) GetCommentsByQuestionId() revel.Result {

	c.Params.Query = c.Request.URL.Query()
	var questionId string
	c.Params.Bind(&questionId, "question_id")

	comments, err := service.GetCommentsByQuestionId(questionId)
	var responseResult model.CommentResponseResult

	if err != nil {
		c.Response.Status = http.StatusInternalServerError
		responseResult.Status = http.StatusInternalServerError
		responseResult.Message = err.Error()
		return c.RenderJSON(responseResult)
	} else {
		c.Response.Status = http.StatusOK
		c.Request.Header.Add("Content-Type", "application/json")
		responseResult.Status = http.StatusOK
		responseResult.Data.Comments = comments
		return c.RenderJSON(responseResult)
	}

}

func (c Comment) InsertComment() revel.Result {
	c.Params.Query = c.Request.URL.Query()
	var comment model.CommentModel
	c.Params.BindJSON(&comment)

	insertedId, err := service.InsertComment(comment)
	var responseResult common.InsertResponseResult

	if err != nil {
		c.Response.Status = http.StatusInternalServerError
		responseResult.Status = http.StatusInternalServerError
		responseResult.Message = err.Error()
		return c.RenderJSON(responseResult)
	} else {
		c.Response.Status = http.StatusOK
		c.Request.Header.Add("Content-Type", "application/json")
		responseResult.Status = http.StatusOK
		responseResult.Data.InsertedId = insertedId
		return c.RenderJSON(responseResult)
	}

}

func (c Comment) UpdateComment() revel.Result {
	c.Params.Query = c.Request.URL.Query()
	var comment model.CommentModel
	c.Params.BindJSON(&comment)

	var commentId string
	c.Params.Bind(&commentId, "comment_id")

	updatedCount, err := service.UpdateComment(commentId, comment)
	var responseResult common.UpdateResponseResult

	if err != nil {
		c.Response.Status = http.StatusInternalServerError
		responseResult.Status = http.StatusInternalServerError
		responseResult.Message = err.Error()
		return c.RenderJSON(responseResult)
	} else {
		c.Response.Status = http.StatusOK
		c.Request.Header.Add("Content-Type", "application/json")
		responseResult.Status = http.StatusOK
		responseResult.Data.UpdatedCount = updatedCount
		return c.RenderJSON(responseResult)
	}
}

func (c Comment) DeleteComment() revel.Result {
	c.Params.Query = c.Request.URL.Query()
	var commentId string
	c.Params.Bind(&commentId, "comment_id")

	deletedCount, err := service.DeleteComment(commentId)
	var responseResult common.DeleteResponseResult

	if err != nil {
		c.Response.Status = http.StatusInternalServerError
		responseResult.Status = http.StatusInternalServerError
		responseResult.Message = err.Error()
		return c.RenderJSON(responseResult)
	} else {
		c.Response.Status = http.StatusOK
		c.Request.Header.Add("Content-Type", "application/json")
		responseResult.Status = http.StatusOK
		responseResult.Data.DeletedCount = deletedCount
		return c.RenderJSON(responseResult)
	}
}
