package controllers

import (
	"InterviewAskApi/app/common"
	"InterviewAskApi/app/model"
	"InterviewAskApi/app/service"
	"net/http"

	"github.com/revel/revel"
)

type Category struct {
	*revel.Controller
}

func (c Category) GetCategories() revel.Result {

	c.Params.Query = c.Request.URL.Query()
	var categoryId string
	c.Params.Bind(&categoryId, "category_id")

	categories, err := service.GetCategories(categoryId)
	// TODO: Getting list of question for each category
	var responseResult model.CategoryResponseResult

	if err != nil {
		c.Response.Status = http.StatusInternalServerError
		responseResult.Status = http.StatusInternalServerError
		responseResult.Message = err.Error()
		return c.RenderJSON(responseResult)
	} else {
		c.Response.Status = http.StatusOK
		c.Request.Header.Add("Content-Type", "application/json")
		responseResult.Status = http.StatusOK
		responseResult.Data.Categories = categories
		return c.RenderJSON(responseResult)
	}

}

func (c Category) InsertCategory() revel.Result {
	c.Params.Query = c.Request.URL.Query()
	var category model.CategoryModel
	c.Params.BindJSON(&category)

	category_check, err := service.FindByCategoryName(category.CategoryName)

	var responseResult common.InsertResponseResult

	if err != nil {
		c.Response.Status = http.StatusInternalServerError
		responseResult.Status = http.StatusInternalServerError
		responseResult.Message = err.Error()
		return c.RenderJSON(responseResult)
	} else {
		if len(category_check) > 0 {
			c.Response.Status = http.StatusConflict
			responseResult.Status = http.StatusConflict
			responseResult.Message = "Category Exists"
			return c.RenderJSON(responseResult)
		} else {

			insertedId, err := service.InsertCategory(category)

			if err != nil {
				c.Response.Status = http.StatusInternalServerError
				responseResult.Status = http.StatusInternalServerError
				responseResult.Message = err.Error()
				return c.RenderJSON(responseResult)
			} else {
				c.Response.Status = http.StatusOK
				c.Request.Header.Add("Content-Type", "application/json")
				responseResult.Status = http.StatusOK
				responseResult.Data.InsertedId = insertedId
				return c.RenderJSON(responseResult)
			}
		}

	}

}

func (c Category) UpdateCategory() revel.Result {
	c.Params.Query = c.Request.URL.Query()
	var category model.CategoryModel
	c.Params.BindJSON(&category)

	var categoryId string
	c.Params.Bind(&categoryId, "category_id")

	updatedCount, err := service.UpdateCategory(categoryId, category)
	var responseResult common.UpdateResponseResult

	if err != nil {
		c.Response.Status = http.StatusInternalServerError
		responseResult.Status = http.StatusInternalServerError
		responseResult.Message = err.Error()
		return c.RenderJSON(responseResult)
	} else {
		c.Response.Status = http.StatusOK
		c.Request.Header.Add("Content-Type", "application/json")
		responseResult.Status = http.StatusOK
		responseResult.Data.UpdatedCount = updatedCount
		return c.RenderJSON(responseResult)
	}
}

func (c Category) DeleteCategory() revel.Result {
	c.Params.Query = c.Request.URL.Query()
	var categoryId string
	c.Params.Bind(&categoryId, "category_id")

	deletedCount, err := service.DeleteCategory(categoryId)
	var responseResult common.DeleteResponseResult

	if err != nil {
		c.Response.Status = http.StatusInternalServerError
		responseResult.Status = http.StatusInternalServerError
		responseResult.Message = err.Error()
		return c.RenderJSON(responseResult)
	} else {
		c.Response.Status = http.StatusOK
		c.Request.Header.Add("Content-Type", "application/json")
		responseResult.Status = http.StatusOK
		responseResult.Data.DeletedCount = deletedCount
		return c.RenderJSON(responseResult)
	}
}
