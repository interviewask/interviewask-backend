package controllers

import (
	"InterviewAskApi/app/common"
	modelService "InterviewAskApi/app/service"
	"net/http"
	"time"

	"github.com/golang-jwt/jwt"
	"github.com/revel/revel"
)

type Login struct {
	*revel.Controller
}

type ResponseResult struct {
	Status  int    `json:"status"`
	Message string `json:"message"`
	Token   string `json:"token"`
	Role    string `json:"role"`
}

type Account struct {
	UserName string `json:"user_name"`
	Password string `json:"password"`
}

var sampleSecretKey = []byte(common.SECRET_KEY)

func (l Login) GetTokenHandler() revel.Result {
	l.Params.Query = l.Request.URL.Query()

	var account Account
	l.Params.BindJSON(&account)

	var result ResponseResult

	user, resultCheck := modelService.CheckUserExists(account.UserName)

	if !resultCheck {
		l.Response.Status = http.StatusUnauthorized
		result.Status = http.StatusUnauthorized
		result.Message = "account incorrect"
		return l.RenderJSON(result)
	}

	if common.CheckPasswordHash(account.Password, user[0].Password) {
		claims := jwt.MapClaims{
			"user_name": user[0].UserName,
			"role":      user[0].Role,
			"ExpiresAt": 5,
			"IssuedAt":  time.Now().Unix(),
		}
		token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
		tokenStr, err := token.SignedString(sampleSecretKey)
		if err != nil {
			l.Response.Status = http.StatusBadGateway
			result.Status = http.StatusBadGateway
			result.Message = err.Error()
			return l.RenderJSON(result)
		}

		l.Response.Status = http.StatusOK
		result.Status = http.StatusOK
		result.Token = tokenStr
		result.Role = user[0].Role
		return l.RenderJSON(result)
	} else {
		l.Response.Status = http.StatusUnauthorized
		result.Status = http.StatusUnauthorized
		result.Message = "account incorrect"
		return l.RenderJSON(result)
	}
}
