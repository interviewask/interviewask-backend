package controllers

import (
	"InterviewAskApi/app/common"
	"InterviewAskApi/app/model"
	"InterviewAskApi/app/service"
	"net/http"

	"github.com/revel/revel"
)

type User struct {
	*revel.Controller
}

func (u User) CheckUserName() revel.Result {

	u.Params.Query = u.Request.URL.Query()
	var userName string
	u.Params.Bind(&userName, "user_name")

	user, err := service.FindByUserName(userName)

	var responseResult model.CheckUserResponseResult

	if err != nil {
		u.Response.Status = http.StatusInternalServerError
		responseResult.Status = http.StatusInternalServerError
		responseResult.Message = err.Error()
		return u.RenderJSON(responseResult)
	} else {
		if len(user) > 0 {
			u.Response.Status = http.StatusConflict
			responseResult.Status = http.StatusConflict
			responseResult.Message = "User Name Exist"
			return u.RenderJSON(responseResult)
		} else {
			u.Response.Status = http.StatusOK
			responseResult.Status = http.StatusOK
			responseResult.Message = "User Name non-exist"
			return u.RenderJSON(responseResult)
		}
	}
}

func (u User) GetUserById() revel.Result {

	u.Params.Query = u.Request.URL.Query()
	var userId string
	u.Params.Bind(&userId, "user_id")

	users, err := service.GetUserById(userId)

	var responseResult model.UserResponseResult

	if err != nil {
		u.Response.Status = http.StatusInternalServerError
		responseResult.Status = http.StatusInternalServerError
		responseResult.Message = err.Error()
		return u.RenderJSON(responseResult)
	} else {
		u.Response.Status = http.StatusOK
		u.Request.Header.Add("Content-Type", "application/json")
		responseResult.Status = http.StatusOK
		responseResult.Data.User = users
		return u.RenderJSON(responseResult)
	}

}

func (u User) GetUsers() revel.Result {

	u.Params.Query = u.Request.URL.Query()
	var userId string
	u.Params.Bind(&userId, "user_id")

	users, err := service.GetUsers()

	var responseResult model.UserResponseResult

	if err != nil {
		u.Response.Status = http.StatusInternalServerError
		responseResult.Status = http.StatusInternalServerError
		responseResult.Message = err.Error()
		return u.RenderJSON(responseResult)
	} else {
		u.Response.Status = http.StatusOK
		u.Request.Header.Add("Content-Type", "application/json")
		responseResult.Status = http.StatusOK
		responseResult.Data.User = users
		return u.RenderJSON(responseResult)
	}

}

func (u User) InsertUser() revel.Result {
	u.Params.Query = u.Request.URL.Query()
	var user model.UserModel
	u.Params.BindJSON(&user)

	insertedId, err := service.InsertUser(user)
	var responseResult common.InsertResponseResult

	if insertedId == "-9" {
		u.Response.Status = http.StatusConflict
		responseResult.Status = http.StatusConflict
		responseResult.Message = "User exists"
		return u.RenderJSON(responseResult)
	} else {
		if err != nil {
			u.Response.Status = http.StatusInternalServerError
			responseResult.Status = http.StatusInternalServerError
			responseResult.Message = err.Error()
			return u.RenderJSON(responseResult)
		} else {
			u.Response.Status = http.StatusOK
			u.Request.Header.Add("Content-Type", "application/json")
			responseResult.Status = http.StatusOK
			responseResult.Data.InsertedId = insertedId
			return u.RenderJSON(responseResult)
		}
	}
}

func (u User) UpdateUser() revel.Result {
	u.Params.Query = u.Request.URL.Query()
	var user model.UserModel
	u.Params.BindJSON(&user)

	var userId string
	u.Params.Bind(&userId, "user_id")

	updatedCount, err := service.UpdateUser(userId, user)
	var responseResult common.UpdateResponseResult

	if err != nil {
		u.Response.Status = http.StatusInternalServerError
		responseResult.Status = http.StatusInternalServerError
		responseResult.Message = err.Error()
		return u.RenderJSON(responseResult)
	} else {
		u.Response.Status = http.StatusOK
		u.Request.Header.Add("Content-Type", "application/json")
		responseResult.Status = http.StatusOK
		responseResult.Data.UpdatedCount = updatedCount
		return u.RenderJSON(responseResult)
	}
}

func (u User) DeleteUser() revel.Result {
	u.Params.Query = u.Request.URL.Query()
	var userId string
	u.Params.Bind(&userId, "user_id")

	deletedCount, err := service.DeleteUser(userId)
	var responseResult common.DeleteResponseResult

	if err != nil {
		u.Response.Status = http.StatusInternalServerError
		responseResult.Status = http.StatusInternalServerError
		responseResult.Message = err.Error()
		return u.RenderJSON(responseResult)
	} else {
		u.Response.Status = http.StatusOK
		u.Request.Header.Add("Content-Type", "application/json")
		responseResult.Status = http.StatusOK
		responseResult.Data.DeletedCount = deletedCount
		return u.RenderJSON(responseResult)
	}
}
