package controllers

import (
	"InterviewAskApi/app/common"
	"InterviewAskApi/app/model"
	"InterviewAskApi/app/service"
	"net/http"

	"github.com/revel/revel"
)

type Question struct {
	*revel.Controller
}

func (q Question) GetQuestions() revel.Result {

	q.Params.Query = q.Request.URL.Query()
	var questionId string
	q.Params.Bind(&questionId, "question_id")

	questions, err := service.GetQuestions(questionId)
	// TODO: Getting comment list for each of question

	var responseResult model.QuestionResponseResult

	if err != nil {
		q.Response.Status = http.StatusInternalServerError
		responseResult.Status = http.StatusInternalServerError
		responseResult.Message = err.Error()
		return q.RenderJSON(responseResult)
	} else {
		q.Response.Status = http.StatusOK
		q.Request.Header.Add("Content-Type", "application/json")
		responseResult.Status = http.StatusOK
		responseResult.Data.Questions = questions
		return q.RenderJSON(responseResult)
	}
}

func (q Question) GetQuestionsByCategoryId() revel.Result {

	q.Params.Query = q.Request.URL.Query()
	var categoryId string
	q.Params.Bind(&categoryId, "category_id")

	questions, err := service.GetQuestionsByCategoryId(categoryId)
	// TODO: Getting comment list for each of question

	var responseResult model.QuestionResponseResult

	if err != nil {
		q.Response.Status = http.StatusInternalServerError
		responseResult.Status = http.StatusInternalServerError
		responseResult.Message = err.Error()
		return q.RenderJSON(responseResult)
	} else {
		q.Response.Status = http.StatusOK
		q.Request.Header.Add("Content-Type", "application/json")
		responseResult.Status = http.StatusOK
		responseResult.Data.Questions = questions
		return q.RenderJSON(responseResult)
	}
}

func (q Question) InsertQuestion() revel.Result {
	q.Params.Query = q.Request.URL.Query()
	var question model.QuestionModel
	q.Params.BindJSON(&question)

	insertedId, err := service.InsertNewQuestion(question)
	var responseResult common.InsertResponseResult

	if err != nil {
		q.Response.Status = http.StatusInternalServerError
		responseResult.Status = http.StatusInternalServerError
		responseResult.Message = err.Error()
		return q.RenderJSON(responseResult)
	} else {
		q.Response.Status = http.StatusOK
		q.Request.Header.Add("Content-Type", "application/json")
		responseResult.Status = http.StatusOK
		responseResult.Data.InsertedId = insertedId
		return q.RenderJSON(responseResult)
	}
}

func (q Question) UpdateQuestion() revel.Result {
	q.Params.Query = q.Request.URL.Query()
	var question model.QuestionModel
	q.Params.BindJSON(&question)

	var questionId string
	q.Params.Bind(&questionId, "question_id")

	updatedCount, err := service.UpdateQuestion(questionId, question)
	var responseResult common.UpdateResponseResult

	if err != nil {
		q.Response.Status = http.StatusInternalServerError
		responseResult.Status = http.StatusInternalServerError
		responseResult.Message = err.Error()
		return q.RenderJSON(responseResult)
	} else {
		q.Response.Status = http.StatusOK
		q.Request.Header.Add("Content-Type", "application/json")
		responseResult.Status = http.StatusOK
		responseResult.Data.UpdatedCount = updatedCount
		return q.RenderJSON(responseResult)
	}
}

func (q Question) UpdateCategoryQuestion() revel.Result {
	q.Params.Query = q.Request.URL.Query()
	var question model.QuestionModel
	q.Params.BindJSON(&question)

	var questionId string
	q.Params.Bind(&questionId, "question_id")

	updatedCount, err := service.UpdateCategoryQuestion(questionId, question)
	var responseResult common.UpdateResponseResult

	if err != nil {
		q.Response.Status = http.StatusInternalServerError
		responseResult.Status = http.StatusInternalServerError
		responseResult.Message = err.Error()
		return q.RenderJSON(responseResult)
	} else {
		q.Response.Status = http.StatusOK
		q.Request.Header.Add("Content-Type", "application/json")
		responseResult.Status = http.StatusOK
		responseResult.Data.UpdatedCount = updatedCount
		return q.RenderJSON(responseResult)
	}
}

func (q Question) DeleteQuestion() revel.Result {
	q.Params.Query = q.Request.URL.Query()
	var questionId string
	q.Params.Bind(&questionId, "question_id")

	deletedCount, err := service.DeleteQuestion(questionId)
	var responseResult common.DeleteResponseResult

	if err != nil {
		q.Response.Status = http.StatusInternalServerError
		responseResult.Status = http.StatusInternalServerError
		responseResult.Message = err.Error()
		return q.RenderJSON(responseResult)
	} else {
		q.Response.Status = http.StatusOK
		q.Request.Header.Add("Content-Type", "application/json")
		responseResult.Status = http.StatusOK
		responseResult.Data.DeletedCount = deletedCount
		return q.RenderJSON(responseResult)
	}
}
