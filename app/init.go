package app

import (
	"InterviewAskApi/app/common"
	"InterviewAskApi/app/controllers"
	"fmt"
	"net/http"
	"strconv"

	"github.com/golang-jwt/jwt"
	_ "github.com/revel/modules"
	"github.com/revel/revel"
)

var (
	// AppVersion revel app version (ldflags)
	AppVersion string

	// BuildTime revel app build-time (ldflags)
	BuildTime string
)

var sampleSecretKey = []byte(common.SECRET_KEY)

func init() {
	// Filters is the default set of global filters.
	// revel.Filters = []revel.Filter{
	// 	revel.PanicFilter,             // Recover from panics and display an error page instead.
	// 	revel.RouterFilter,            // Use the routing table to select the right Action
	// 	revel.FilterConfiguringFilter, // A hook for adding or removing per-Action filters.
	// 	revel.ParamsFilter,            // Parse parameters into Controller.Params.
	// 	revel.SessionFilter,           // Restore and write the session cookie.
	// 	revel.FlashFilter,             // Restore and write the flash cookie.
	// 	revel.ValidationFilter,        // Restore kept validation errors and save new ones from cookie.
	// 	revel.I18nFilter,              // Resolve the requested language
	// 	HeaderFilter,                  // Add some security based headers
	// 	revel.InterceptorFilter,       // Run interceptors around the action.
	// 	revel.CompressFilter,          // Compress the result.
	// 	revel.BeforeAfterFilter,       // Call the before and after filter functions
	// 	revel.ActionInvoker,           // Invoke the action.
	// }

	revel.InterceptFunc(AuthenticateUser, revel.BEFORE, &controllers.Question{})
	revel.InterceptFunc(AuthenticateUser, revel.BEFORE, &controllers.User{})
	revel.InterceptFunc(AuthenticateUser, revel.BEFORE, &controllers.Category{})
	revel.InterceptFunc(AuthenticateUser, revel.BEFORE, &controllers.Comment{})

	// Register startup functions with OnAppStart
	// revel.DevMode and revel.RunMode only work inside of OnAppStart. See Example Startup Script
	// ( order dependent )
	// revel.OnAppStart(ExampleStartupScript)
	// revel.OnAppStart(InitDB)
	// revel.OnAppStart(FillCache)
}

// HeaderFilter adds common security headers
// There is a full implementation of a CSRF filter in
// https://github.com/revel/modules/tree/master/csrf
var HeaderFilter = func(c *revel.Controller, fc []revel.Filter) {
	c.Response.Out.Header().Add("X-Frame-Options", "SAMEORIGIN")
	c.Response.Out.Header().Add("X-XSS-Protection", "1; mode=block")
	c.Response.Out.Header().Add("X-Content-Type-Options", "nosniff")
	c.Response.Out.Header().Add("Referrer-Policy", "strict-origin-when-cross-origin")

	fc[0](c, fc[1:]) // Execute the next filter stage.
}

// THANG ADDED
func AuthenticateUser(c *revel.Controller) revel.Result {

	// Authorize Question API
	if ac, ok := c.AppController.(*controllers.Question); ok {
		if !isElementExist(common.UNAUTHORIZED_METHODS, ac.MethodName) {
			accessToken := ac.Request.GetHttpHeader("Access-Token")
			status, msg, isAuthenticated := CheckTokenHandler(accessToken, ac.MethodName)
			if !isAuthenticated {
				response := make(map[string]string)
				response["status"] = strconv.Itoa(status)
				response["message"] = msg
				ac.Response.Status = status
				return c.RenderJSON(response)
			} else {
				return nil
			}
		} else {
			return nil
		}
	}

	// Authorize User API
	if ac, ok := c.AppController.(*controllers.User); ok {
		if !isElementExist(common.UNAUTHORIZED_METHODS, ac.MethodName) {
			accessToken := ac.Request.GetHttpHeader("Access-Token")
			status, msg, isAuthenticated := CheckTokenHandler(accessToken, ac.MethodName)
			if !isAuthenticated {
				response := make(map[string]string)
				response["status"] = strconv.Itoa(status)
				response["message"] = msg
				ac.Response.Status = status
				return c.RenderJSON(response)
			} else {
				return nil
			}
		} else {
			return nil
		}
	}

	// Authorize Category API
	if ac, ok := c.AppController.(*controllers.Category); ok {
		if !isElementExist(common.UNAUTHORIZED_METHODS, ac.MethodName) {
			accessToken := ac.Request.GetHttpHeader("Access-Token")
			status, msg, isAuthenticated := CheckTokenHandler(accessToken, ac.MethodName)
			if !isAuthenticated {
				response := make(map[string]string)
				response["status"] = strconv.Itoa(status)
				response["message"] = msg
				ac.Response.Status = status
				return c.RenderJSON(response)
			} else {
				return nil
			}
		} else {
			return nil
		}
	}

	// Authorize Comment API
	if ac, ok := c.AppController.(*controllers.Comment); ok {
		if !isElementExist(common.UNAUTHORIZED_METHODS, ac.MethodName) {
			accessToken := ac.Request.GetHttpHeader("Access-Token")
			status, msg, isAuthenticated := CheckTokenHandler(accessToken, ac.MethodName)
			if !isAuthenticated {
				response := make(map[string]string)
				response["status"] = strconv.Itoa(status)
				response["message"] = msg
				ac.Response.Status = status
				return c.RenderJSON(response)
			} else {
				return nil
			}
		} else {
			return nil
		}
	}
	return nil
}

func CheckTokenHandler(tokenStr string, methodName string) (status int, message string, isAuthenticated bool) {
	if len(tokenStr) == 0 {
		status := http.StatusForbidden
		msg := "Can not find token in header"
		isAuthenticated := false
		return status, msg, isAuthenticated
	}

	token, err := jwt.Parse(tokenStr, func(t *jwt.Token) (interface{}, error) {
		if _, ok := t.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("Unexpected signing method: %v", t.Header["alg"])
		}
		return sampleSecretKey, nil
	})

	if err != nil {
		status := http.StatusForbidden
		msg := "Access Denied; Please check the access token"
		isAuthenticated := false
		return status, msg, isAuthenticated
	}

	if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
		if isElementExist(common.ADMIN_METHODS, methodName) {
			if claims["role"] != common.ADMIN_ROLE {
				status := http.StatusForbidden
				msg := "Access Denied; Please check the access token"
				isAuthenticated := false
				return status, msg, isAuthenticated
			} else {
				status := http.StatusOK
				msg := "User Authenticated!!"
				isAuthenticated := true
				return status, msg, isAuthenticated
			}
		} else {
			status := http.StatusOK
			msg := "User Authenticated!!"
			isAuthenticated := true
			return status, msg, isAuthenticated
		}

	} else {
		status := http.StatusForbidden
		msg := "Access Denied; Please check the access token"
		isAuthenticated := false
		return status, msg, isAuthenticated
	}
}

func isElementExist(s []string, str string) bool {
	for _, v := range s {
		if v == str {
			return true
		}
	}
	return false
}

//func ExampleStartupScript() {
//	// revel.DevMod and revel.RunMode work here
//	// Use this script to check for dev mode and set dev/prod startup scripts here!
//	if revel.DevMode == true {
//		// Dev mode
//	}
//}
